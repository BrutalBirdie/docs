#!/bin/bash

set -eu

export MKDOCS_MATERIAL_VERSION="9.5.36"
export MKDOCS_REDIRECTS_VERSION="1.2.1"
export SURFER_VERSION="6.0.0"

echo "=> Installing mkdocs-material"
python3 -m venv ./.python-venv
./.python-venv/bin/pip install mkdocs-material==${MKDOCS_MATERIAL_VERSION} mkdocs-redirects==${MKDOCS_REDIRECTS_VERSION} pillow cairosvg

echo "=> Installing @redocly/cli"
npm install -g @redocly/cli

echo "=> Installing surfer"
npm install -g cloudron-surfer@${SURFER_VERSION}

echo "=> Updating Docker Image"
docker build . -t cloudron/docs-ci --build-arg MKDOCS_MATERIAL_VERSION=$MKDOCS_MATERIAL_VERSION --build-arg MKDOCS_REDIRECTS_VERSION=$MKDOCS_REDIRECTS_VERSION --build-arg SURFER_VERSION=$SURFER_VERSION
docker push cloudron/docs-ci

echo ""
echo "=> Done."
echo "=> To use the new docker image, change the sha in the .gitlab-ci.yml <="
echo ""
