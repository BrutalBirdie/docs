definitions:
  Dashboard:
    description: Configurations of your cloudron server.
    type: object
    properties:
      apiServerOrigin:
        type: string
        example: "https://api.cloudron.io"
      webServerOrigin:
        type: string
        example: "https://cloudron.io"
      consoleServerOrigin:
        type: string
        example: "https://console.cloudron.io"
      adminDomain:
        type: string
        example: "${YOUR_ADMIN_DOMAIN}"
      adminFqdn:
        type: string
        example: "${YOUR_ADMIN_FQDN}"
      mailFqdn:
        type: string
        example: "${YOUR_MAIL_FQDN}"
      version: 
        type: string
        example: "8.0.0"
      ubuntuVersion:
        type: string
        example: "Ubuntu 22.04.4 LTS"
      isDemo:
        type: boolean
        example: false
      cloudronName:
        type: string
        example: "Your Cloudron Instance"
      footer:
        type: string
        example: >
          "Thanks for using Cloudron!"
      features:
        type: object
        properties:
          userMaxCount:
            type: boolean
            example: true
          userGroups:
            type: boolean
            example: true
          userRoles: 
            type: boolean
            example: true
          domainMaxCount:
            example: null
          externalLdap:
            type: boolean
            example: false
          privateDockerRegistry:
            type: boolean
            example: false
          branding:
            type: boolean
            example: true
          support:
            type: boolean
            example: true
          directoryConfig:
            type: boolean
            example: true
          profileConfig:
            type: boolean
            example: true
          mailboxMaxCount:
            type: boolean
            example: true
          emailPremium:
            type: boolean
            example: true
      profileLocked:
        type: boolean
        example: false
      mandatory2FA:
        type: boolean
        example: false
      external2FA:
        type: boolean
        example: false

/dashboard/config:
  get:
    operationId: getConfig
    summary: Get config 
    description: Get config
    tags: [ "Dashboard" ]
    security:
      - bearer_auth: [ 'read' ]
      - query_auth: [ 'read' ]
    responses:
      200:
        description: Success
        content:
          application/json:
            schema:
              type: object
              properties:
                dashboard:
                  type: array
                  items:
                    $ref: '#/definitions/Dashboard'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl \
          -H "Content-Type: application/json" \
          -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/dashboard/config"

/dashboard/startPrepareLocation:
  post:
    operationId: startPrepareLocation
    summary: Set dashboard location
    description: Set your dashboard location.
    tags: [ "Dashboard" ]
    security:
      - bearer_auth: [ 'write' ]
      - query_auth: [ 'write' ]
    requestBody:
      required: true
      content:
        application/json:
          schema:
            properties:
              domain:
                type: string
                example: example.org
                description: |
                  The domain where the dashboard will be located at. The `my.` can't be changed. With the given example it would look like this: `my.example.org`.
            required:
              - domain
    responses:
      204:
        description: Success
        content:
          application/json:
            schema:
              type: object
              properties:
                $ref: '../schemas.yaml#/task_info/properties'
      400:
        $ref: '../responses.yaml#/bad_field'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      409:
        $ref: '../responses.yaml#/conflict'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl -X POST \
          -H "Content-Type: application/json" \
          -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/dashboard/startPrepareLocation" --data '{"domain": "example.org"}'

/dashboard/location:
  post:
    operationId: changeLocation
    summary: Change dashboard location
    description: Change the location of your Cloudron dashboard.
    tags: [ "Dashboard" ]
    security:
      - bearer_auth: [ 'write' ]
      - query_auth: [ 'write' ]
    requestBody:
      required: true
      content:
        application/json:
          schema:
            properties:
              domain:
                type: string
                example: anotherExample.org
                description: |
                  The domain where the dashboard will be located at. The `my.` can't be changed. With the given example it would look like this: `my.anotherExample.org`.
            required:
              - domain
    responses:
      204:
        $ref: '../responses.yaml#/no_content'
      400:
        $ref: '../responses.yaml#/bad_field'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      409:
        $ref: '../responses.yaml#/conflict'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl -X POST \
          -H "Content-Type: application/json" \
          -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/dashboard/location" --data '{"domain": "anotherExample.org"}'