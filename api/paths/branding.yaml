definitions:
  Footer:
    type: string
    description: |
      The footer is a markdown string. It can be templated with the following variables:
        * `%YEAR%` - the current year
        * `%VERSION%` - current Cloudron version
    example: 'All Rights Reserved (c) 2010-%YEAR%'

/branding/cloudron_avatar:
  get:
    operationId: getCloudronAvatar
    summary: Get Cloudron Avatar (icon)
    description: Get the Cloudron Avatar. The Cloudron avatar (icon) is used in Email templates, Dashboard header and the Login pages.
    tags: [ "Branding" ]
    security:
      - bearer_auth: [ 'read' ]
      - query_auth: [ 'read' ]
    responses:
      200:
        description: Success
        content:
          image/png:
            schema:
              type: string
              format: binary
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl -H "Content-Type: application/json" -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/branding/cloudron_avatar"

  post:
    operationId: setCloudronAvatar
    summary: Set Cloudron Avatar (icon)
    description: Set the Cloudron Avatar. The Cloudron avatar (icon) is used in Email templates, Dashboard header and the Login pages.
    tags: [ "Branding" ]
    security:
      - bearer_auth: [ 'write' ]
      - query_auth: [ 'write' ]
    requestBody:
      required: true
      content:
        multipart/form-data:
          schema:
            type: object
            properties:
              avatar:
                type: string
                format: binary
            required:
              - avatar
    responses:
      200:
        $ref: '../responses.yaml#/no_content'
      400:
        $ref: '../responses.yaml#/bad_field'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/branding/cloudron_name" --form avatar=@localfilename.png

/branding/cloudron_name:
  get:
    operationId: getCloudronName
    summary: Get Cloudron Name
    description: Get the Cloudron Name. The Cloudron name is used in Email templates, Dashboard header and the Login pages.
    tags: [ "Branding" ]
    security:
      - bearer_auth: [ 'read' ]
      - query_auth: [ 'read' ]
    responses:
      200:
        description: Success
        content:
          application/json:
            schema:
              type: object
              properties:
                name:
                  type: string
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl -H "Content-Type: application/json" -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/branding/cloudron_name"

  post:
    operationId: setCloudronName
    summary: Set Cloudron Name
    description: Set the Cloudron Name. The Cloudron name is used in Email templates, Dashboard header and the Login pages.
    tags: [ "Branding" ]
    security:
      - bearer_auth: [ 'write' ]
      - query_auth: [ 'write' ]
    requestBody:
      required: true
      content:
        application/json:
          schema:
            properties:
              name:
                type: string
                minimum: 1
                maximum: 64
            required:
              - name
    responses:
      200:
        $ref: '../responses.yaml#/no_content'
      400:
        $ref: '../responses.yaml#/bad_field'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl -X POST -H "Content-Type: application/json" -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/branding/cloudron_name" --data '{"name":"My Home Cloud"}'

/branding/cloudron_background:
  get:
    operationId: getCloudronBackgroundBranding
    summary: Get Cloudron Background
    description: Get the Cloudron Background.
    tags: [ "Branding" ]
    security:
      - bearer_auth: [ 'read' ]
      - query_auth: [ 'read' ]
    responses:
      200:
        description: Success
        content:
          application/json:
            schema:
              type: object
              properties:
                name:
                  type: string
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl -H "Content-Type: application/json" -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/branding/cloudron_name"
  post:
    operationId: setCloudronBackground
    summary: Set Cloudron background image
    tags: [ "Branding" ]
    security:
      - bearer_auth: [ 'read' ]
      - query_auth: [ 'read' ]
    requestBody:
      required: true
      content:
        multipart/form-data:
          schema:
            type: object
            properties:
              background:
                type: string
                format: binary
                description: The background image file to be set. If not provided, the background image will be cleared.
    responses:
      202:
        $ref: '../responses.yaml#/no_content'
      400:
        $ref: '../responses.yaml#/bad_field'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl \
          -H "Content-Type: application/json" \
          -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/branding/cloudron_background"

/branding/footer:
  get:
    operationId: getCloudronFooter
    summary: Get Cloudron Footer
    description: Get the Cloudron Footer. The Cloudron Footer is used in the Dashboard and Login pages.
    tags: [ "Branding" ]
    security:
      - bearer_auth: [ 'read' ]
      - query_auth: [ 'read' ]
    responses:
      200:
        description: Success
        content:
          application/json:
            schema:
              type: object
              properties:
                footer:
                  $ref: '#/definitions/Footer'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl -H "Content-Type: application/json" -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/branding/footer"

  post:
    operationId: setCloudronFooter
    summary: Set Cloudron Footer
    description: Set the Cloudron Footer. The Cloudron Footer is used in the Dashboard and Login pages.
    tags: [ "Branding" ]
    security:
      - bearer_auth: [ 'write' ]
      - query_auth: [ 'write' ]
    requestBody:
      required: true
      content:
        application/json:
          schema:
            properties:
              footer:
                $ref: '#/definitions/Footer'
            required:
              - footer
    responses:
      200:
        $ref: '../responses.yaml#/no_content'
      400:
        $ref: '../responses.yaml#/bad_field'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl -X POST -H "Content-Type: application/json" -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/branding/footer" --data '{"footer":"All Rights Reserved"}'
