/provision/setup:
    post:
      operationId: setupProvision
      summary: Configure inital domain
      description: API endpoint to configure domain settings. To read more about the existing domain provider and their domain configs inside the **Domain Route**.
      tags: [ "Provision" ]
      security:
      - bearer_auth: [ 'write' ]
      - query_auth: [ 'write' ]
      requestBody:
        required: true
        content:
          application/json:
            schema:
              type: object
              properties:
                domainConfig:
                  type: object
                  required:
                    - provider
                    - domain
                    - config
                  properties:
                    provider:
                      type: string
                      description: The provider of the domain.
                    domain:
                      type: string
                      description: The domain name.
                    zoneName:
                      type: string
                      description: The zone name.
                    config:
                      type: object
                      description: Configuration settings for the domain.
                    tlsConfig:
                      type: object
                      description: TLS configuration settings.
                      properties:
                        provider:
                          type: string
                          description: The provider of the TLS configuration.
                ipv4Config:
                  type: object
                  description: IPv4 configuration settings.
                ipv6Config:
                  type: object
                  description: IPv6 configuration settings.
      responses:
        200:
          $ref: '../responses.yaml#/no_content'
        400:
          $ref: '../responses.yaml#/bad_field'
        401:
          $ref: '../responses.yaml#/unauthorized'
        403:
          $ref: '../responses.yaml#/forbidden'
        500:
          $ref: '../responses.yaml#/server_error'
      x-codeSamples:
        - lang: cURL
          source: |-
            curl -X POST \
              -H "Content-Type: application/json" \
              -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/provision/setup" --data '{"domainConfig": {"provider": "aws53", "domain": "","zoneName":"", "config":{}, "tlsConfig":{}} "ipv4Config": {}, "ipv6Config": {}}'

/provision/restore:
  post:
    summary: Restore from backup
    operationId: restoreProvision
    description: Resotre
    tags: [ "Provision" ]
    security:
    - bearer_auth: [ 'write' ]
    - query_auth: [ 'write' ]
    requestBody:
      required: true
      content:
        application/json:
          schema:
            type: object
            properties:
              backupConfig:
                type: object
                required:
                  - provider
                  - format
                  - password
                properties:
                  provider:
                    type: string
                    description: The backup service provider (e.g., AWS, Azure).
                    example: "AWS"
                  format:
                    type: string
                    description: The format of the backup file (e.g., tar, zip).
                    example: "tar"
                  password:
                    type: string
                    description: The password used to encrypt the backup.
                    example: "securepassword123"
              encryptedFilenames:
                type: boolean
                example: true
                description: Enables or disables encrypted file names.
              remotePath:
                type: string
                description: The remote path where backups are stored (e.g., s3://mybucket/backups/).
                example: "s3://mybucket/backups/"
              version:
                type: string
                description: The version of the provisioning script or configuration.
                example: "1.0.0"
              ipv4Config:
                type: object
                description: Configuration settings for IPv4.
              ipv6Config:
                type: object
                description: Configuration settings for IPv6.
              skipDnsSetup:
                type: boolean
                description: Enable or disable to skip the DNS setup.
                example: false
    responses:
      200:
        $ref: '../responses.yaml#/no_content'
      400:
        $ref: '../responses.yaml#/bad_field'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl -X POST \
            -H "Content-Type: application/json" \
            -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/provision/restore" --data '{"backupConfig": {"provider": "aws53", "password": "strongPassword","format":"targz"}, "encryptedFilenames": true, "remotePath": "your.remote.path", "version": "v1.02.2", ipv4Config": {}, "ipv6Config": {}, "skipDnsSetup": false}'

/provision/activate:
  post:
    summary: Create admin
    operationId: activateProvision
    description: Activate your provision.
    tags: [ "Provision" ]
    security:
    - bearer_auth: [ 'write' ]
    - query_auth: [ 'write' ]
    requestBody:
      required: true
      content:
        application/json:
          schema:
            type: object
            properties:
              username:
                type: string
                description: The unique identifier for the user, typically used for login.
                example: "john_doe"
              password:
                type: string
                description: The user's password, used for authentication. This should be stored securely.
                example: "securePassword123!"
              email:
                type: string
                description: The user's email address, used for account recovery and notifications.
                example: "john.doe@example.com"
              displayName:
                type: string
                description: The name displayed publicly for the user, such as in user profiles or posts.
                example: "John Doe"
    responses:
      201:
        description: Success
        content:
          application/json:
            schema:
              type: object
              properties:
                userId:
                  type: string
                  description: The unique identifier associated with the user.
                  example: "123456789"
                token:
                  type: string
                  description: The authentication token used for user authentication.
                  example: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvbiBEb2UiLCJpYXQiOjE1MTYyMzkwMjJ9.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c"
                expires:
                  type: string
                  description: The expiration date and time of the authentication token.
                  example: "2024-12-31T23:59:59Z"
      400:
        $ref: '../responses.yaml#/bad_field'
      401:
        $ref: '../responses.yaml#/unauthorized'
      403:
        $ref: '../responses.yaml#/forbidden'
      500:
        $ref: '../responses.yaml#/server_error'
    x-codeSamples:
      - lang: cURL
        source: |-
          curl -X POST \
            -H "Content-Type: application/json" \
            -H "Authorization: Bearer $CLOUDRON_TOKEN" "https://$CLOUDRON_DOMAIN/api/v1/provision/activate" --data '{"username": "john_doe", "password": "securePassword123!", "email": "john.doe@example.com", "displayName": "John Doe"}'
