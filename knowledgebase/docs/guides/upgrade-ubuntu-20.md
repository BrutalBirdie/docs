# Upgrading to Ubuntu 20.04 (Focal Fossa)

## Overview

In this guide, we will see how to upgrade an existing Ubuntu 18.04 based Cloudron to Ubuntu 20.04.
If you are still on Ubuntu 16, you must first upgrade to Ubuntu 18 before upgrading to Ubuntu 20.
Follow [this guide](/guides/upgrade-ubuntu-18/) to upgrade to Ubuntu 18.

Cloudron 7.5 is the last version of Cloudron that supports Ubuntu 18.04.

## Checklist

Before upgrading, please note the following:

* Cloudron has to be on atleast version 6.0. This can be verified by checking the version in the Settings view. Cloudron releases prior to 6.0 do not support Ubuntu 20.04.
* Ubuntu has to be on version 18.04. Check the output of `lsb_release -a` to confirm this.
* The upgrade takes around 1-3 hours based on various factors like network/cpu/disk etc

## Pre-flight

Before starting the upgrade process, it's a good idea to create a server snapshot to rollback quickly. If your VPS does not have snapshotting feature, it's best to create a full Cloudron backup before attempting the upgrade (Backups -> Create Backup now).

!!! warning "Highly recommend taking server snapshot"
    Taking a server snapshot will save a lot of trouble if your server is unable to start up after a failed Ubuntu upgrade. If your VPS provider does not have snapshot feature, it is critical to have backups _outside_ of your server. This is because in corner cases Ubuntu fails to boot entirely and your data and backups will be locked up inside the server disk.

!!! warning "Digital Ocean"
    We have received numerous complaints about Digital Ocean droplet upgrades from Ubuntu 18 to Ubuntu 20 . It results in a completely hosed droplet where you cannot even SSH after upgrade. We recommend simply starting with a fresh ubuntu 22.04 instance and [restoring](/backups/#restore-cloudron).

## Upgrading

Start the upgrade:

```
# dpkg --configure -a
# apt update
# apt upgrade
# do-release-upgrade
```

Upgrade notes:

* Accept running an additional ssh deamon at port 1022
* For all packages (nginx, timesyncd, journald etc), select N or O  : keep your currently-installed version. This is the 'default'.
* You can pick the default for all questions asked in the upgrade process. Like LXD version can be 4.0 etc.
* Accept removal of obsolete packages.
* **IMPORTANT:** On some VPS, the upgrade process will uninstall MySQL 5.7. It is OK to uninstall it, but be sure to select the option to **not** remove MySQL data.
* On some VPS, the upgrade fails to start with an error like "After updating your package information, the essential package'ubuntu-minimal' could not be located.". This is because the mirror in `/etc/apt/sources.list` has [some issue](https://www.digitalocean.com/community/questions/can-t-upgrade-from-18-04-lts-to-20-04-lts). To rectify, replace that file with the [original source.list](https://gist.github.com/h0bbel/4b28ede18d65c3527b11b12fa36aa8d1).

Once upgrade is complete, restart the server.

## Post Upgrade

* Fixup collectd. `/etc/default/collectd` must have the following line (Add the line to end, if it doesn't exist):

```
LD_PRELOAD=/usr/lib/python3.8/config-3.8-x86_64-linux-gnu/libpython3.8.so
```

* Install MySQL 8. 

```
# systemctl stop box
# apt remove mysql-server-5.7 # this is only needed, if it wasn't removed in the upgrade process above. if it asks, then be sure to preserve MySQL data. if it doesn't, it preserves it by default.
# apt install mysql-server-8.0
# /home/yellowtent/box/setup/start.sh
# systemctl start box
# reboot
```

## Post Upgrade Checks

* `lsb_release -a` will output Ubuntu 20.04.
* `systemctl status box` will say `active (running)`.
* `systemctl status collectd` will say `active (running)`.
* Verify that all the services in the Services view of Cloudron dashboard are running.

