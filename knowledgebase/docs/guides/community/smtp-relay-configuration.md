# SMTP Relay Configuration

## Overview

The following are examples for setting up a [SMTP Relay](/email/#relay-outbound-mails) to relay outbound mails.

!!! warning "Community Guide"
    This guide was contributed by the Cloudron Community, and does not imply support, warranty. You should utilize them at your own risk. You should be familiar with technical tasks, ensure you have backups, and throughly test.

* **Contributor**: [JLX89](https://forum.cloudron.io/user/jlx89)
* [Discuss this guide](https://forum.cloudron.io/category/4/support)

## ZeptoMail

[Zeptomail](https://www.zoho.com/zeptomail/) is a Transaction email service provided by [Zoho](https://zoho.com), which offers a reliable and secure service that takes care of delivering these all-important transactional emails instantly, while you can focus on building your business.

1. Navigate to your email settings in your Cloudron instance. You can do this by clicking on your username in the top left corner and clicking "email", or by going directly to: ```https://my.{server-url}/#/email```

2. Next, click on the domain you'd like configure, and the `Outbound` tab, or going directly to: ```https://my.{server-url}/#/email/{domain-name}/outbound```

### Create API Key

1. Go to ZeptoMail Console
2. Mail Agents > {mail-agent-name}
3. Setup Info > SMTP > `Send Mail API Token #`

### Cloudron Configuration

* Select `External SMTP Server`
* SMTP Host: `smtp.zeptomail.com`
* SMTP Port: 587
* Username: emailapikey
* Password: zeptomail-smtp-api-key

