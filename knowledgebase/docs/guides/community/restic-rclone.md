# Remote backup of local Cloudron backup snapshots with restic & rclone

This is what I use for remote backups of my local Cloudron backup snapshots (done by rsync) via restic / rclone to Onedrive.

[restic](https://restic.net/) is a robust backup solution for incremental, encrypted, mountable(!) backups to local and remote storage. [rclone](https://rclone.org/), an equally robust sync software, is just a "transporter tool" that expands the available remote storages by a lot.

Maybe it can be a starting point and some inspiration for your personal needs.

* **Contributor**: [necrevistonnezr](https://forum.cloudron.io/user/necrevistonnezr)

#### Tools

* rclone: https://rclone.org/docs/
* restic: https://restic.readthedocs.io/en/stable/030_preparing_a_new_repo.html#other-services-via-rclone
* ssmtp: https://wiki.archlinux.org/title/SSMTP

#### Installation

* Install tools above via apt
* afterwards update to latest version (repo versions are old): `sudo restic self-update && sudo rclone selfupdate`

#### Setup rclone

* Enter an interactive setup process via `rclone config`
* in my case I use Onedrive as it has 1TB of space coming with my Office 365 subscription
* for the rest of this summary, we assume you gave it the repository name "REPOSITORY"
* details at https://rclone.org/commands/rclone_config/

#### Setup restic

* set up a backup repository `restic -r rclone:REPOSITORY init` (for compression support add `--repository-version 2`- recommended!)
* for a subfolder on onedrive just use `restic -r rclone:REPOSITORY:subfolder init` (for compression support add `--repository-version 2` - recommended!)
* save password that you gave the repository in file `/home/USER/resticpw`
* details at https://restic.readthedocs.io/en/latest/030_preparing_a_new_repo.html#other-services-via-rclone

#### Setup SSMTP

* for receiving backup results, otherwise not needed
* See https://wiki.archlinux.org/title/SSMTP

#### Cloudron Backup settings

* Provider: mountpoint
* Location: `/media/CloudronBackup` (<-- obviously adjust to your settings)
* this creates a snapshot at `/media/CloudronBackup/snapshot` for the current backup
* Storage Format: rsync
* Adjust schedule and retention to your liking

#### Backup, Prune and Check scripts

###### `restic-cron-backup.sh`: The actual backup

``` sh
#!/bin/bash
d=$(date +%Y-%m-%d)
if pidof -o %PPID -x “$0”; then
echo “$(date “+%d.%m.%Y %T”) Exit, already running.”
exit 1
fi
restic -r rclone:REPOSITORY:subfolder backup /media/CloudronBackup/snapshot -p=/home/USER/resticpw 
restic -r rclone:REPOSITORY:subfolder forget --keep-monthly 12 --keep-weekly 5 --keep-daily 14 -p=/home/USER/resticpw
restic -r rclone:REPOSITORY:subfolder check --read-data-subset=2% -p=/home/USER/resticpw
exit
```
First line does the backup (incremental, encrypted), second line is the backup retention, third line checks a random 2 % of all data for errors.
Note that I only backup the `/snapshot` folder as all versioning is done by restic.
For compression, add `--compression auto` (or `max`) to the `backup` command.

###### `restic-cron-prune.sh`: Pruning unused files in the backup

``` sh
#!/bin/bash
d=$(date +%Y-%m-%d)
if pidof -o %PPID -x “$0”; then
echo “$(date “+%d.%m.%Y %T”) Exit, already running.”
exit 1
fi
restic -r rclone:REPOSITORY:subfolder prune -p=/home/USER/resticpw
exit
```
removes unused data from the repository, I run this once a week

###### `restic-cron-check.sh`: thorough health check of the backups

``` sh
#!/bin/bash
d=$(date +%Y-%m-%d)
if pidof -o %PPID -x “$0”; then
echo “$(date “+%d.%m.%Y %T”) Exit, already running.”
exit 1
fi
restic -r rclone:REPOSITORY:subfolder check --read-data -p=/home/USER/resticpw
exit
```
checks all data for errors, I run this once a week

#### Crontab

``` sh
30 2 * * * sh /home/USER/restic-cron-backup.sh | mailx -s "Restic Backup Results" server@mydomain.com
1 5 1 * * sh /home/USER/restic-cron-prune.sh | mailx -s "Restic Prune Results" server@mydomain.com
1 8 1 * * sh /home/USER/restic-cron-check.sh | mailx -s "Restic Full Check Results" server@mydomain.com
```
Backup daily at 2:30, prune and check once a week. Receive results to specified mail

#### Mount backups

Just to be complete: You can mount restic backups locally like
`restic -r rclone:REPOSITORY:subfolder mount /media/resticmount/ -p=/home/USER/resticpw && cd /media/resticmount`
obviously adjust `/media/resticmount/`to your settings; allows you to browse and copy from full snapshots for each backup

#### List backups

For listing all available snapshots use 
`restic -r rclone:REPOSITORY:subfolder snapshots -p=/home/USER/resticpw`

#### Migrate existing backups to compressed backups

For migrating existing repos to compressed repos use these two steps (will take long!)

* `restic -r rclone:REPOSITORY:subfolder migrate upgrade_repo_v2 -p=/home/USER/resticpw`
* `restic -r rclone:REPOSITORY:subfolder prune --repack-uncompressed -p=/home/USER/resticpw`

See https://restic.readthedocs.io/en/latest/045_working_with_repos.html#upgrading-the-repository-format-version for details.

