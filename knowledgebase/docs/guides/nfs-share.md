# NFS Share

## Overview

It can be sometimes useful to expose application data as a NFS share. For example, you might want to copy
a large number of media files into (or from) Emby/Jellyfin or copy images into Surfer etc.

In this guide, we will see how to expose directories on Cloudron via NFS.

!!! warning "Insecure traffic"
    Please note that NFS traffic is unencrypted and can be tampered. For this reason, you must use NFS mounts only
    on secure private networks.

## Install NFS Server

To install, run the following command on Cloudron server:

```
sudo apt install nfs-kernel-server
```

## Disable NFSv3

By default, the NFS server support v3 and v4. v3 has various [security implications](https://community.netapp.com/t5/Tech-ONTAP-Blogs/NFSv3-and-NFSv4-What-s-the-difference/ba-p/441316)
and we recommend disabling it.

* Edit `/etc/default/nfs-kernel-server` and add:

```
RPCNFSDOPTS="-N 2 -N 3"
```

* Restart the server using `systemctl restart nfs-kernel-server`

* Verify it got disabled by checking `cat /proc/fs/nfsd/versions`

```
-2 -3 +4 +4.1 +4.2
```

* Disable the `rpcbind` service. This is required only for NFSv3.

```
systemctl disable rpcbind.socket rpcbind.service
systemctl stop rpcbind.socket rpcbind.service
```

## Exposing a directory

Edit `/etc/exports` and add a line like so:

```
# this exposes data of the app with id appid to the Client IP address client_ip
/home/yellowtent/appsdata/app_id/data client_ip(rw,sync,no_subtree_check,no_root_squash)
```

Meaning of the options:

* `rw`: can read and write to volume
* `sync`: server replies to requests only after the changes have been committed to stable storage
* `no_subtree_check`: when exporting subdirectories, the server skips checking if every file access is still in the originally exported filesystem
* `no_root_squash`: the root user of client is mapped to root user on server as well.

## Export the directory

Export the NFS directory that we configured above:

```
exportfs -a
systemctl restart nfs-kernel-server
```

## Expose NFS port

Port 2049 (TCP/UDP) is used for NFS traffic. Add this to [Cloudron Firewall](/networking/#whitelist-ports)
by editing `/home/yellowtent/platformdata/firewall/ports.json`:

```
{
    "allowed_tcp_ports": [ 2049 ],
    "allowed_udp_ports": [ 2049 ]
}
```

Restart the firewall to apply the configuration:

```
systemctl restart cloudron-firewall
```

## Mounting on client

Add the following entry to your laptop/PC's `/etc/fstab`:

```
cloudron_ip:/home/yellowtent/appsdata/app_id/data               /mounts/app      nfs auto,nofail,noatime,nolock,intr,tcp,actimeo=1800 0 0
```
