# Settings

## System Time Zone

The System Time Zone setting controls various cron jobs like backup, updates, date display in emails etc. This time zone can be changed from the settings view.

<center>
<img src="/img/settings-timezone.png" class="shadow" width="500px">
</center>

!!! warning "Server is UTC"
    The System time zone setting does not change the Server's timezone. The server should always be in UTC. This ensures
    all Database timestamps and logs are in UTC. This is intentional and should not be changed.

!!! note "Browser timestamps"
    For convenience, when viewing logs using the [log viewer](/apps/#log-viewer), timestamps are converted into
    the browser timezone.

## Language

The default language of Cloudron is English. This can be changed using the Language settings.
When set, users will be sent out invitation and reset emails using the selected language.
Note that users can always set a different language in their [profile](/profile/#language).

<center>
<img src="/img/settings-language.png" class="shadow" width="500px">
</center>


## Private Docker Registry

A private docker registry can be setup to pull the docker images of [custom apps](/custom-apps/tutorial/).

<center>
<img src="/img/settings-private-registry.png" class="shadow" width="500px">
</center>

!!! note "Use docker.io and not docker.com"
    If you are using the private DockerHub registry, use `docker.io` and not `docker.com`

