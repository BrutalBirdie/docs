# Dashboard

## Filter

Apps in the dashboard can be filtered using one or more of the following filters:

* Group name
* App State
* Domain name

<center>
<img src="../img/dashboard-filter.png" class="shadow" width="250px">
</center>

