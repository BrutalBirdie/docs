# App Store

## Overview

The Cloudron App Store is a repository of apps hosted at [cloudron.io](https://cloudron.io). The App Store
provides app packages that can be installed on a Cloudron. A Cloudron installation
periodically polls the App Store for updates.

## Account

A Cloudron App Store account (cloudron.io account) is used to manage your subscription & billing. Before installing
apps, you must set up the Cloudron with your App Store Account information. By doing so, the
Cloudron will register itself and get an unique Cloudron ID.

You can view this information in the `Settings` page.

<br/>

<center>
<img src="/img/cloudron-account.png" class="shadow" width="500px">
</center>

## Password reset

The password of the App Store account can be reset <a href="https://cloudron.io/passwordreset.html" target="_blank">here</a>.

## Account Change

To switch the App Store account (cloudron.io account) to a new email address, you can simply change the email address in
[your cloudron.io profile](https://console.cloudron.io/#/account).

It's not possible to transfer subscriptions to another existing cloudron.io account. If really needed, the workflow for
account transfer is as follows:

* Send the Cloudron ID(s) to transfer with the email address of your old account to support@cloudron.io
* We will cancel your existing subscription and refund the pro-rated amount
* You can then [delete the subscription](https://console.cloudron.io/#/subscriptions) from your old account.
* When you delete a subscription, Cloudron Dashboard's App Store view will log you put. Login with new cloudron.io account
  and re-set up the subscription.

