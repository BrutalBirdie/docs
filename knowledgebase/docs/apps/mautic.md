# <img src="/img/mautic-logo.png" width="25px"> Mautic App

## About

Mautic a free and opensource marketing automation tool.

* Questions? Ask in the [Cloudron Forum - Mautic](https://forum.cloudron.io/category/46/mautic)
* [Mautic Website](https://www.mautic.org/)
* [Mautic forum](https://forum.mautic.org/)
* [Mautic issue tracker](https://github.com/mautic/mautic/issues)

## System cron jobs

Mautic often requires cron jobs to be triggered manually. The package comes with a a default
cronjob template. This is saved in `/app/data/crontab.system`.

You can change the schedule and arguments of the cronjobs as desired using the [File manager](/apps/#file-manager).
Be sure to restart the app after making any changes.

To run a cron job manuall, open a [Web terminal](/apps/#web-terminal) and run the job manually like so:

```
sudo -E -u www-data php /app/code/bin/console mautic:segments:update
```

## Custom cron jobs

For cron support, app custom commands in the app's [cron section](/apps/#cron).

The crontab contains a line like:

```
0,15,30,45 * * * *  sudo -E -u www-data php /app/code/app/console mautic:integration:fetchleads --integration=Hubspot
```

## Mailer

Mautic 5 supports sending emails via [Symfony mailer](https://symfony.com/doc/current/mailer.html). The following
mailer packages are pre-installed:

* symfony/amazon-mailer
* symfony/mailgun-mailer
* symfony/postmark-mailer
* symfony/sendgrid-mailer
* symfony/mailjet-mailer

!!! warning "Disable email auto-configuration"
    On Cloudron, mail settings are managed by the platform. By default, mautic will send messages via the
    built-in Cloudron Email Server. If you want to change the mail configuration
    to another mailer, you can either set it at the server level as an [outbound relay](/email/#relay-outbound-mails)
    or at the app level by [disabling email configuration](/apps/#disable-email-configuration).

## Clearing cache

To clear the cache, you can either delete `/run/mautic/var/cache` or you can run the following command
using the [Web terminal](/apps/#web-terminal):

```
sudo -E -u www-data php /app/code/bin/console cache:clear
```

## Admin

### Admin password reset

To reset the admin user password, run the following command using the [Web terminal](/apps/#web-terminal):
```
set-admin-password <newstrongpassword>
```

### Admin email reset

To set the admin user email, run the following command using the [Web terminal](/apps/#web-terminal):
```
set-admin-email <email@example.com>
```
