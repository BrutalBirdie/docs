# <img src="/img/libretranslate-logo.png" width="25px"> LibreTranslate App

## About

Free and Open Source Machine Translation API. Self-hosted, offline capable and easy to setup.

* Questions? Ask in the [Cloudron Forum - LibreTranslate](https://forum.cloudron.io/category/156/libretranslate)
* [LibreTranslate Website](https://libretranslate.com/)
* [LibreTranslate issue tracker](https://github.com/LibreTranslate/LibreTranslate/issues)

## Languages

By default the app comes with EN and DE installed. If more languages are wanted, update `LT_LANGUAGE_MODELS` variable in `/app/data/env.sh`, and restart the app.
e.g.
```
export LT_LANGUAGE_MODELS=en,de,fr
```

To remove languages, update `LT_LANGUAGE_MODELS` variable appropriately, remove `/app/data/argos-translate` folder and restart the app.

## User Management

The app has no user management of it's own. By default, anyone (without a token) can make 30 requests per minute. You can change this in `/app/data/env.sh`.

## Custom config

[Custom environment variables](https://github.com/LibreTranslate/LibreTranslate#settings--flags) can be set in `/app/data/env.sh` using the [File manager](/apps/#file-manager).

## API

By default, the API can be used without a key to make 30 requests per minute:

```
$ curl -d "q=cloudron%20ist%20wunderbar&source=de&target=en" -X POST https://translate.domain.com/translate
```

To lock the app and enforce use of a key, set `LT_REQUIRE_API_KEY_ORIGIN=true` in `/app/data/env.sh`

### Create Key

Use the [ltmanage](https://github.com/LibreTranslate/LibreTranslate/blob/main/README.md#manage-api-keys) command to create an API key. On Cloudron, LibreTranslate runs as the `cloudron` user. Run the following command using a [Web Terminal](/apps/#web-terminal) to create an API with a limit of 120 requests per minute:

```
# su - cloudron -c 'source /app/code/.venv/bin/activate; ltmanage keys --api-keys-db-path /app/data/db/api_keys.db add 120'
f67d5afe-aa14-4d37-90f7-f7d9636b721e
```

The key can be used as follows:

```
$ curl -d "q=cloudron%20ist%20wunderbar&api_key=f67d5afe-aa14-4d37-90f7-f7d9636b721e&source=de&target=en" -X POST https://translate.domain.com/translate
```

