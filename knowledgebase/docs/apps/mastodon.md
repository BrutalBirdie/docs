# <img src="/img/mastodon-logo.png" width="25px"> Mastodon App

## About

Mastodon is an open source decentralized social network - by the people for the people. Join the federation and take back control of your social media!

* Questions? Ask in the [Cloudron Forum - Mastodon](https://forum.cloudron.io/category/41/mastodon)
* [Mastodon Website](https://joinmastodon.org/)
* [Mastodon issue tracker](https://github.com/tootsuite/mastodon/issues/)

## Admin

Mastodon 4 introduced [customizable roles](https://github.com/mastodon/mastodon/pull/18641).

To make a user an administrator, use the [Web Terminal](/apps/#web-terminal) and run the following command:

```
/app/code/bin/tootctl accounts modify <username> --role Owner
```

Note that there is also a default `Admin` role. See [this PR](https://github.com/mastodon/mastodon/pull/18641) for more information.

Once made an owner/admin, you will `Administration` under `Preferences` (at `/admin/dashboard`).

## Adding users

When used with Cloudron authentication, simply add new users to the Cloudron dashboard. As of this writing, mixing Cloudron users and external users does not work well (See [this issue](https://github.com/mastodon/mastodon/issues/20655)).

Without Cloudron authentication, new users can be added and approved using the CLI:

```
/app/code/bin/tootctl accounts create testusername --email=test@cloudron.io --approve
```

## Username restriction

Usernames can contain only letters, numbers and underscores. On Cloudron, usernames can be
created with hyphen and dot. Such usernames cannot login to Mastodon and will get a "HTTP ERROR 422" when
attempting to login. Please see the [upstream bugreport](https://github.com/mastodon/mastodon/issues/12020) for more information.

## Registration

Registration is closed by default. To enable, login to Mastodon as an admin and change the "Registration mode" under `Administration` -> `Site Settings`.

!!! warning "Does not work well with Cloudron user management"
    Various features like password reset, re-invitation etc do not work well because of [upstream bug](https://github.com/mastodon/mastodon/issues/20655).


## Federation

Cloudron will setup Mastodon accounts to be of the form `username@social.example.org` when you install
Mastodon at `social.example.org`. This domain is called the `LOCAL_DOMAIN` in Mastodon terminology.

Changing the `LOCAL_DOMAIN` will let you have handles as `username@example.org` even when installed at `social.example.org`
Changing the `LOCAL_DOMAIN` is not recommended since it is complicated and in most cases unnecessary.
This is because Mastodon account names are not intended to be remembered like usernames (it's not like email where
you can start following another account). Instead, users usually visit a website and click the 'Follow' button.

If you decide to not change the `LOCAL_DOMAIN`, no further configuration is required and your Mastodon instance
is already set up for federation.

### Changing LOCAL_DOMAIN

You can change the account domain name by using the [File Manager](/apps/#file-manager)
and changing `LOCAL_DOMAIN` in `/app/data/env.production`. After that, you have to configure `LOCAL_DOMAIN`'s
web server to serve up `.well-known/host-meta` query.

If `LOCAL_DOMAIN` is an app on Cloudron, you can use Cloudron's Well Known URI
support. Go to the `Domains` view and set the Mastodon domain in the `Advanced` settings:

<center>
<img src="/img/mastodon-wellknown.png" class="shadow" width="500px">
</center>

If the `LOCAL_DOMAIN` is **NOT** hosted on Cloudron, you must figure out a suitable way to serve up the well-known documents.
Here are some hints:

* For WordPress, you can setup a redirect using [Redirection plugin](https://wordpress.org/plugins/redirection/)
* For Ghost,you can add a [redirects.json](https://ghost.org/tutorials/implementing-redirects/)
* For Surfer, simply upload the XML above into `.well-known/host-meta`.
* For anything else, setup nginx config as follows:

```
location = /.well-known/host-meta {
       return 301 https://social.example.org$request_uri;
}
```

## Following users

To follow external users, visit their mastodon account and click on 'Follow'. This will popup a window asking your
mastodon identity (which will be `username@LOCAL_DOMAIN`).

If you have an existing account on another server, you can bring those connections with you to your own server.
For this, go to Settings -> Data Export and download your following list as a CSV file, and finally
on your own server, you go to Settings -> Import and upload that file.

## Scaling

Various parameters in Puma, Streaming and Sidekiq can be fine-tuned for concurrency. To change the
parameters, edit `/app/data/config.sh` using the [File manager](/apps/#file-manager) and restart the app.
See [scaling docs](https://docs.joinmastodon.org/admin/scaling/) for more information.

Sample configuration for `/app/data/config.sh` (from [here](https://forum.cloudron.io/topic/8629/mastodon-scaling])):

```
# Puma
export WEB_CONCURRENCY=4 # number of worker processes
export MAX_THREADS=5 # the number of threads per process

# Streaming API
export STREAMING_CLUSTER_NUM=2 # number of worker processes

# Sidekiq
export SIDEKIQ_THREADS=4
export DB_POOL=25 # must be at least the same as the number of threads
```

## S3 for media storage

Media storage can consume quite a bit of space. To move it to S3 or S3 compatible provider, edit `/app/data/env.production` using
the [File manager](/apps/#file-manager) and add something like this:

```
S3_ENABLED=true
S3_BUCKET=bucket-name
AWS_ACCESS_KEY_ID=<key_id>
AWS_SECRET_ACCESS_KEY=<secret_key>
S3_REGION=<region>
S3_PROTOCOL=https
S3_HOSTNAME=<hostname>
```

See [this thread](https://forum.cloudron.io/topic/8330/backing-up-media-cache) for more information.

## Cache retention days

The app will clean various federation caches regularily. Depending on the usage of an instance,
the days cached resources should be keept can be configured via `CACHE_RETENTION_DAYS` in the `cache-env.sh` file.

By default all cached assets are kept for 2 days.
