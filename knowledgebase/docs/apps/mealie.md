# <img src="/img/mealie-logo.png" width="25px"> Mealie App

## About

Mealie is a self hosted recipe manager and meal planner with a RestAPI backend and a reactive frontend application built in Vue for a pleasant user experience for the whole family. Easily add recipes into your database by providing the url and mealie will automatically import the relevant data or add a family recipe with the UI editor

* Questions? Ask in the [Cloudron Forum - Mealie](https://forum.cloudron.io/category/161/mealie)
* [Mealie Website](https://mealie.io/)
* [Mealie issue tracker](https://github.com/hay-kot/mealie/issues)
