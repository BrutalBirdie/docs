# <img src="/img/minio-logo.png" width="25px"> MinIO App

## About

Minio is a high performance S3 compatible Object Storage.

* Questions? Ask in the [Cloudron Forum - Minio](https://forum.cloudron.io/category/69/minio)
* [Minio Website](http://www.minio.io)
* [Minio issue tracker](https://github.com/minio/minio/issues)

## Admin credentials

To change admin credentials, use the [File manager](/apps#file-manager) to edit
the variables `MINIO_ROOT_USER` and `MINIO_ROOT_PASSWORD` in `/app/data/env.sh` .

```
export MINIO_ROOT_USER=superadmin
export MINIO_ROOT_PASSWORD='Secr$et#pass'
```

Be sure to restart the app after making changes.

## Default SSO Policy

By default, Cloudron users have `readwrite` access policy. To change it:

* Create a policy by following [Minio documentation](https://min.io/docs/minio/linux/administration/identity-access-management/policy-based-access-control.html)
* Add the variable `MINIO_IDENTITY_OPENID_ROLE_POLICY` in `/app/data/env.sh` using the [File manager](/apps/#file-manager).

```
export MINIO_IDENTITY_OPENID_ROLE_POLICY="new-policy-name"
```

Be sure to restart the app and re-login for changes to take effect.

## Domains

MinIO uses two domains:

* Console domain - this domain is for accessing the MinIO console.

* API Domain - this endpoint responds to S3 API requests. This is the domain that you need to put into various configs and tools like s3cmd. Only the domain name is needed, no port should be added.

In the screenshot below, you would use `minio.cloudron.site` as the console domain. You can login and view your files. The `minio-api.cloudron.site` is the API domain which responds to API requests.

<center>
<img src="/img/minio-two-domains.png" class="shadow" width="500px">
</center>

## Cloudron Backup

Cloudron supports [backing up to minio](/backups/#minio).
Backing up a Cloudron to a minio installed in another Cloudron will work fine. However, backing up a
Cloudron to a minio installed in the very same Cloudron is not supported.

## Custom configuration

Custom config variables can be exported in `/app/data/env.sh`. This file is sourced automatically on startup.

## Access keys

Access Key and Secret Key can be created in two ways in the `Identity` section.

* Create a user - The access key is the username and the corresponding secret key is the password.
* Create a service account - These are accounts with policies but cannot login to the MinIO dashboard.

<center>
<img src="/img/minio-service-account.png" class="shadow" width="500px">
</center>

## CLI

MinIO supports multiple long term users in addition to default user created during server startup. New users
have to added using the CLI tool. You can read the full docs [here](https://docs.min.io/docs/minio-multi-user-quickstart-guide.html).

* [Install Minio CLI tool](https://docs.min.io/docs/minio-client-quickstart-guide.html)

* Configure CLI tool to point to your minio installation
```
    mc alias set myminio https://minio.cloudron.club minioadmin minioadmin --api s3v4
```

* Create a policy file
```
cat > getonly.json << EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "s3:GetObject"
      ],
      "Effect": "Allow",
      "Resource": [
        "arn:aws:s3:::my-bucketname/*"
      ],
      "Sid": ""
    }
  ]
}
EOF
```

* Add the policy

```
   mc admin policy add myminio getonly getonly.json
```

* Add new user

```
   mc admin user add myminio newuser password123
```

* Set policy on user

```
    mc admin policy set myminio getonly user=newuser
```


