# <img src="/img/pairdrop-logo.png" width="25px"> PairDrop App

## About

Local file sharing in your browser. Inspired by Apple's AirDrop. Fork of Snapdrop. 

* Questions? Ask in the [Cloudron Forum - PairDrop](https://forum.cloudron.io/category/193/pairdrop)
* [Pairdrop Website](https://github.com/schlagmichdoch/PairDrop)
* [PairDrop Issue Tracker](https://github.com/schlagmichdoch/PairDrop/issues)
* [PairDrop Discussions](https://github.com/schlagmichdoch/PairDrop/discussions)

