# <img src="/img/pixelfed-logo.png" width="25px"> Pixelfed App

## About

A free and ethical photo sharing platform, powered by ActivityPub federation.

* Questions? Ask in the [Cloudron Forum - Pixelfed](https://forum.cloudron.io/category/90/pixelfed)
* [Pixelfed Website](https://pixelfed.org/)
* [Pixelfed docs](https://docs.pixelfed.org/)
* [Pixelfed issue tracker](https://github.com/pixelfed/pixelfed/issues)

## Admin

To make a register user an admin, use the [Web terminal](/apps#web-terminal) and run
the following command:

```
# sudo -E -u www-data php artisan user:admin username_here

Found username: girish

 Add admin privileges to this user? (yes/no) [no]:
 > yes

Successfully changed permissions!
```

Further administration commands like removing a user, removing unused media can be found in
[Pixelfed docs](https://docs.pixelfed.org/running-pixelfed/administration/#remove-unused-media).

The admin page is located at `https://pixelfed.example.com/i/admin/dashboard`.

## Federation

To test if federation works, search for a handle like `@girish@pixelfed.social` or `@social@pixelfed.cloudron.io`. You should then
be able to follow that handle. Note that you can only see posts that are made _after_ you followed
the handle. Existing posts will **not** appear in you stream.

If following doesn't work:

* Login as an admin user on your Pixelfed instance
* Check `https://pixelfed.domain.com/horizon/failed` for job failures 

## Customizations

Customizations can be made by editing `/app/data/env.production` using the [File manager](/apps/#file-manager). See
the [Pixelfed docs](https://docs.pixelfed.org/technical-documentation/config/) on the various config options.

Don`t forget to restart the app after making any changes.

## Admin Panel

When making changes via the admin panel, often the cache has to be cleared in addition to an app restart. To clear
the cache, open a [Web Terminal](/apps/#web-terminal) and run the following commands:

```
# sudo -E -u www-data php artisan cache:clear
# sudo -E -u www-data php artisan optimize:clear
# sudo -E -u www-data php artisan optimize
```

