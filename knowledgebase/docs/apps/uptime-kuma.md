# <img src="/img/uptime-kuma-logo.png" width="25px"> Uptime Kuma App

## About

Uptime Kuma is a self-hosted monitoring tool like "Uptime Robot".

* [Website](https://github.com/louislam/uptime-kuma)
* [Issue tracker](https://github.com/louislam/uptime-kuma/issues)

## Custom config

[Custom environment variables](https://github.com/louislam/uptime-kuma/wiki/Environment-Variables)
can be set by editing `/app/data/env` using the [File manager](/apps/#file-manager). Be sure to restart the app after making
any changes.

Example:

```
export UPTIME_KUMA_DISABLE_FRAME_SAMEORIGIN=true
```
