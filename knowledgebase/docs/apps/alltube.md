# <img src="/img/alltube-logo.png" width="25px"> Alltube App

## About

Alltube provides a Web GUI for youtube-dl. Despite it's name, Alltube can be used to download
videos from all kinds of sources including Dailymotion, Vimeo, SoundCloud, Facebook and Instagram.

* Questions? Ask in the [Cloudron Forum - AllTube](https://forum.cloudron.io/category/63/alltube)
* [AllTube Website](https://alltubedownload.net)
* [AllTube issue tracker](https://github.com/Rudloff/alltube/issues)

## Customization

Use the [File Manager](/apps#file-manager) to edit `/app/data/config.yml` to add custom configuration.
See the [upstream file](https://github.com/Rudloff/alltube/blob/master/config/config.example.yml)
for reference.

