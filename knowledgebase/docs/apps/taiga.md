# <img src="/img/taiga-logo.png" width="25px"> Taiga App

## About

Taiga is the project management tool for multi-functional agile teams.

* Questions? Ask in the [Cloudron Forum - Taiga](https://forum.cloudron.io/category/65/taiga)
* [Taiga Website](https://taiga.io)
* [Taiga issue tracker](https://github.com/taigaio/taiga-front/issues)

## Custom configuration

Taiga customizations are placed in two files:

* `conf.json` - This contains [UI settings](https://github.com/taigaio/taiga-front/blob/master/conf/conf.example.json). On the Cloudron app, this file is
  located at `/app/data/conf.json`.

* `local.py` - This contains [backend settings](https://github.com/taigaio/taiga-back/blob/master/settings/config.py.prod.example). On the Cloudron app, this file
  is located at `/app/data/customlocal.py`. The default settings are listed in the [common.py](https://github.com/taigaio/taiga-back/blob/master/settings/common.py) file.

These customizations will persist across updates and restarts.

To edit these files use the [File manager](/apps#file-manager).

## Disabling external registration

When using Cloudron auth, external registration is already disabled. When now
using Cloudron auth, edit the following files using [File manager](/apps#file-manager).

* `PUBLIC_REGISTER_ENABLED = False` in `/app/data/customlocal.py`
* `"publicRegisterEnabled": false` in `/app/data/conf.json`

## Importing a project

An existing project's json can be imported into Taiga as follows:

* Connect to taiga using the [Web terminal](/apps#web-terminal)
* Upload the project.json using the 'Upload to /tmp' button
* `su cloudron`
* `source /app/code/taiga/bin/activate`
* `export DJANGO_SETTINGS_MODULE=settings.config`
* `cd /app/code/taiga-back`
* `python manage.py load_dump /tmp/project.json email@domain.tld`

### Asana

To import projects from Asana:

* Create an [Asana Connect](https://asana.com/guide/help/api/api#gl-connect) client and secret. The redirect URL must be set to `https://taiga.example.com/project/new/import/asana`

* Edit the backend config at `/app/data/customlocal.py` to contain the key:

```
IMPORTERS["asana"] = {
    "active": True, # Enable or disable the importer
    "callback_url": "{}://{}/project/new/import/asana".format(SITES["front"]["scheme"],
                                                              SITES["front"]["domain"]),
    "app_id": "client id from above",
    "app_secret": "client secret from above"
}
```

* Edit `/app/data/conf.json`:

```
  "importers": [
    "asana"
  ]
```

* Restart the app

## Plugins

### Slack

The `slack` plugin is installed but not enabled. To enable it, add the following line to `/app/data/customlocal.py`:

```
INSTALLED_APPS += ["taiga_contrib_slack"]
```

Enable the frontend of Slack plugin by editing `/app/data/conf.json`:

```
    "contribPlugins": [ "/plugins/slack/slack.json" ]
```

Then, initialize the database by running the following command in the [web terminal](/apps#web-terminal):

```
source /app/code/taiga/bin/activate
cd /app/code/taiga-back
python manage.py migrate taiga_contrib_slack
```

Be sure to restart the app for the changes to take effect.

