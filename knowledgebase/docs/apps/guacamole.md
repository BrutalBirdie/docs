# <img src="/img/guacamole-logo.png" width="25px"> Guacamole App

## About

Apache Guacamole is a clientless remote desktop gateway. It supports standard protocols like VNC, RDP, and SSH.

* Questions? Ask in the [Cloudron Forum - Guacamole](https://forum.cloudron.io/category/99/guacamole)
* [Guacamole Website](https://guacamole.apache.org//)
* [Guacamole support](https://guacamole.apache.org/support/)
* [Guacamole issue tracker](https://issues.apache.org/jira/projects/GUACAMOLE/issues/GUACAMOLE-1239)

## RDP

Most Windows/RDP servers do not have a valid certificate installed. For this reason,
be sure to check the ignore server certificate checkbox in the Parameters section.

<center>
<img src="/img/guacamole-rdp.png" class="shadow" width="500px">
</center>

## Guacamole menu

The Guacamole menu is a sidebar which is hidden until explicitly shown. On a desktop or other
device which has a hardware keyboard, you can show this menu by pressing **Ctrl+Alt+Shift**.
If you are using a mobile or touchscreen device that lacks a keyboard, you can also show the
menu by swiping right from the left edge of the screen.

See [the docs](https://guacamole.apache.org/doc/gug/using-guacamole.html#guacamole-menu) for
more information.

## Extensions

Extensions can be downloaded from the Apache Guacamole [releases](http://guacamole.apache.org/releases) page.
They are packaged as tar.gz and when extracted have a jar file inside them.

To add an extension:

* Upload the jar file (and not .tar.gz) to `/app/data/extensions` using the [File manager](/apps/#file-manager)
* Restart Guacamole

## Custom properties

Some extensions require adding [custom properties](https://guacamole.apache.org/doc/gug/configuring-guacamole.html). You can add these using the [File manager](/apps/#file-manager)
by editing `/app/data/guacamole.properties`. Be sure to restart the app after you make changes.

## Branding

To change the logo and style, you have to use an extension. You can start with the jar file [in this repo](https://github.com/Zer0CoolX/guacamole-customize-loginscreen-extension).

* Download `branding.jar`
* Upload it using the [File manager](/apps/#file-manager) to `/app/data/extensions`
* Restart Guacaomole

The login screen should already be different now. You can now modify the `branding.jar` as needed to make changes.
Note that a `jar` file is just a zip archive. Just extract it, change files and compress it again. When re-creating
the jar file, make sure that the files are at the top level and not inside a sub directory.


