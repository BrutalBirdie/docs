# <img src="/img/documize-logo.png" width="25px"> Documize App

## About

Documize Community is an open source, modern, self-hosted, enterprise-grade knowledge management solution.

* Questions? Ask in the [Cloudron Forum - Documize](https://forum.cloudron.io/category/150/documize)
* [Documize Website](https://documize.com)
* [Contact](https://www.documize.com/contact)


