# <img src="/img/commento-logo.png" width="25px"> Commento++ App

## About

Commento++ is a fast, privacy-focused commenting platform. Comment++ is a fork of Commento.

* Questions? Ask in the [Cloudron Forum - Commento++](https://forum.cloudron.io/category/40/commento)
* [Original Commento Website](https://commento.io/)
* [Commento++ Website](https://github.com/souramoo/commentoplusplus)

## Registration

Commento++ does not integrate with Cloudron Directory. It has two types of users:

1. **Owners** can manage domains and act as moderators.
2. **Commentors** can write comments.

Owner signup is enabled by default. To disable open owner registration, edit `/app/data/commento.conf` using
the [File manager](/apps/#file-manager):

```
COMMENTO_FORBID_NEW_OWNERS=true
```

Be sure to restart the app after making any changes.

## Sign-in with Google

Commento++ also supports OAuth sign-in with Google for users wanting to comment.
To enable this feature, create a Google API secret and key pair and put those into `/app/data/commento.conf`:

```
COMMENTO_GOOGLE_KEY=<your key>
COMMENTO_GOOGLE_SECRET=<your secret>
```

