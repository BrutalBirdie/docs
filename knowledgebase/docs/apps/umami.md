# <img src="/img/umami-logo.png" width="25px"> Umami App

## About

Umami is a simple, fast, privacy-focused alternative to Google Analytics. 

* Questions? Ask in the [Cloudron Forum - Umami](https://forum.cloudron.io/category/141/umami)
* [Umami Website](https://umami.is/)
* [Umami docs](https://umami.is/docs/about)
* [Umami issue tracker](https://github.com/mikecao/umami/issues)

## Admin password

To change the password of a user, open a [Web Terminal](/apps/#web-terminal):

```
root@b47ad085-8e41-4fcf-8b08-b7b78f83e15a:/app/code# export DATABASE_URL=${CLOUDRON_POSTGRESQL_URL}
root@b47ad085-8e41-4fcf-8b08-b7b78f83e15a:/app/code# node scripts/change-password.js
✔ Enter account to change password … admin
✔ Enter new password … ******
✔ Confirm new password … ******
Password changed for user admin
```

## Custom config

[Custom configuration](https://umami.is/docs/environment-variables) can be added to `/app/data/env.sh`
using the [File manager](/apps/#file-manager).

Be sure to restart the app after adding any custom configuration.

## Custom endpoint

To set a custom script name edit  `/app/data/env.sh` using the [File manager](/apps/#file-manager):

```
export TRACKER_SCRIPT_NAME=clickcounter
```

To set a custom API endpoint:

```
export COLLECT_API_ENDPOINT=/api/counter
```

Be sure to restart the app after making any changes.

