# <img src="/img/documenso-logo.png" width="25px"> Documenso App

## About

The Open Source DocuSign Alternative.

* Questions? Ask in the [Cloudron Forum - Documenso](https://forum.cloudron.io/category/192/documenso)
* [Documenso Website](https://documenso.com/)
* [Documenso issue tracker](https://github.com/RocketChat/Rocket.Chat/issues)

## Disable registration

After you create a user for yourselves you should switch user registration off by setting `NEXT_PUBLIC_DISABLE_SIGNUP` variable to `false` in `/app/data/.env` and restarting the app.

```
NEXT_PUBLIC_DISABLE_SIGNUP=true
```

!!! warning OpenID with signup
    `NEXT_PUBLIC_DISABLE_SIGNUP` also stops OIDC from creating new user accounts.

## Signing certificate

For the digital signature of your documents you need a signing certificate in .p12 format (public and private key).
You can create a self-signed one with the following commands in the [web terminal](/apps#web-terminal) into the app:

Generate a private key using the OpenSSL command. You can run the following command to generate a 2048-bit RSA key:
```
cd /run
openssl genrsa -out private.key 2048
```

Generate a self-signed certificate using the private key. You can run the following command to generate a self-signed certificate:
```
openssl req -new -x509 -key private.key -out certificate.crt -days 365
```
This will prompt you to enter some information, such as the Common Name (CN) for the certificate. Make sure you enter the correct information. The -days parameter sets the number of days for which the certificate is valid.

Combine the private key and the self-signed certificate to create the p12 certificate. You can run the following command to do this:
```
openssl pkcs12 -export -out cert.p12 -inkey private.key -in certificate.crt -legacy
```

You will be prompted to enter a password for the p12 file. If you choose to set an optional password, you have to put the passphrase into the `.env` file:
```
NEXT_PRIVATE_SIGNING_PASSPHRASE={YourStrongPassHere}
```

Place the certificate at `/app/data/resources/cert.p12`:
```
mv /run/cert.p12 /app/data/resources/cert.p12
chown cloudron:cloudron /app/data/resources/cert.p12
```

Then restart the app.
