# <img src="/img/audiobookshelf-logo.png" width="25px"> Audiobookshelf App

## About

Audiobookshelf is a self-hosted audiobook and podcast server.

* Questions? Ask in the [Cloudron Forum - Audiobookshelf](https://forum.cloudron.io/category/167/audiobookshelf)
* [Audiobookshelf repo](https://git.cloudron.io/cloudron/audiobookshelf-app)
* [Audiobookshelf issue tracker](https://github.com/advplyr/audiobookshelf/issues)

## Library

The package auto creates two directories to store files:

* `/app/data/podcasts` - for podcasts
* `/app/data/audiobooks` - for audiobooks

You can upload content into above folders using the [File manager](/apps/#file-manager).

  <center>
  <img src="/img/audiobookshelf-add-library.png" class="shadow" width="500px">
  </center>


!!! note "Enter path manually"
    Library names do not appear properly when clicking `Browse for Folder` button. Instead,
    add paths in the input box manually like above.

