# <img src="/img/mattermost-logo.png" width="25px"> Mattermost App

## About

Mattermost is an open source, self-hosted Slack-alternative.

* Questions? Ask in the [Cloudron Forum - Mattermost](https://forum.cloudron.io/category/26/mattermost)
* [Mattermost Website](https://mattermost.org/)
* [Mattermost forum](https://forum.mattermost.org/)
* [Mattermost docs](https://docs.mattermost.com/index.html)
* [Mattermost issue tracker](https://github.com/mattermost/mattermost-server/issues)

## Config

The config file is located at `/app/data/config.json` and can be edited using the [File manager](/apps/#file-manager).
Be sure to restart the app after making changes to the config file.

## Command Line Tool

The [Mattermost CLI tool](https://docs.mattermost.com/manage/mmctl-command-line-tool.html) can be used
to administer user and team management tasks.

Starting Mattermost 6.0, the CLI tool has to installed locally on your Mac/PC. See the [CLI docs](https://docs.mattermost.com/manage/mmctl-command-line-tool.html#install-mmctl) for downloading the binaries.

Once installed, you can run mmctl like so:

```
$ /app/code/mmctl auth login https://mattermost.cloudron.space --name myserver --username admin
Password:

  credentials for "myserver": "admin@https://mattermost.cloudron.space" stored
```

You can then run commands like this `mmctl team list` etc.

## Migrate External Instance

In you want to migrate your existing non-Cloudron mattermost installation to Cloudron, use the bulk export/import functionality of mmctl.

Caveats:

* plugins, integrations, webhook, custom message formats, system settings have to be [setup again](https://github.com/mattermost/mattermost/issues/18351#issuecomment-1200870881)
* User passwords are not migrated. All users have to reset their password in the new instance.

### Export External Instance

[Export data](https://docs.mattermost.com/manage/bulk-export-tool.html) from the old installation using `mmctl`.
`mmctl` can be installed on your PC/laptop.

```
$ mmctl auth login https://mmold.instance
$ mmctl export create
Export process job successfully created, ID: 8qxfhwmrejnkij3rbqzpo1q7wy
$ mmctl export list
8qxfhwmrejnkij3rbqzpo1q7wy_export.zip
$ mmctl export download 8qxfhwmrejnkij3rbqzpo1q7wy_export.zip
```

### Import to Cloudron Instance

Install mattermost on Cloudron. Because we don't have any users yet to login and authenticate, we must use the local socket to authenticate with mattermost.

* Change `EnableLocalMode` to true in `/app/data/config.json` and `LocalModeSocketLocation` to `/tmp/mattermost_local.socket` using the [File manager](/apps/#file-manager)
  and restart the app.

* Open a [Web Terminal](/apps#web-terminal) .

* Upload the export file to `/tmp'.

* Import using the mmctl binary.

```
$ cd /tmp
$ curl -L https://releases.mattermost.com/mmctl/v9.8.0/linux_amd64.tar | tar xvf -
$ export MMCTL_LOCAL_SOCKET_PATH=/tmp/mattermost_local.socket
$ ./mmctl --local import process --bypass-upload /tmp/8qxfhwmrejnkij3rbqzpo1q7wy_export.zip
Import process job successfully created, ID: ksy4gtmxqpbgumcw7hd84dcr6r
$ ./mmctl --local import job list
  ID: ksy4gtmxqpbgumcw7hd84dcr6r
  Status: success
  Created: 2024-05-31 11:33:09 +0000 UTC
  Started: 2024-05-31 11:33:19 +0000 UTC
  Data: map[extract_content:true import_file:/tmp/8qxfhwmrejnkij3rbqzpo1q7wy_export.zip local_mode:true]
```

* Change `EnableLocalMode` to false in `/app/data/config.json` and restart the app.

## Enterprise

To switch to the Enterprise version of Mattermost, edit `/app/data/edition.ini` using the [File manager](/apps/#file-manager)
and change `edition=enterprise`.

Be sure to restart the app after making any changes.

## Migrate to Postgres

Mattermost has [decided](https://docs.mattermost.com/deploy/postgres-migration.html) to deprecate MySQL support and focus on PostgreSQL support. For this reason, we have made a new package that uses PostgreSQL with appstore id `org.mattermost.cloudronapp2` and deprecated the old MySQL package (appstore id `org.mattermost.cloudronapp`).

There are two ways to migrate: Database migration and Export/Import.

### Database Migration

For this, use the [upstream guide](https://docs.mattermost.com/deploy/postgres-migration.html) as the reference.

!!! note "Use forum for help"
    Please note that we are not experts on Mattermost or it's schema. Database migration is a complex topic and it is unfortunate
    that upstream has decided to deprecate support for MySQL. For this reason, please use the [Cloudron Forum](https://forum.cloudron.io/category/26/mattermost) or the upstream [Mattermost forum](https://forum.mattermost.com/) to ask for migration help.

`pgloader` tool is used to migrate data from MySQL to PostgreSQL. The migration config for pgloader is located at `/app/pkg/migration.load` . This file is copied from [the upstream docs](https://docs.mattermost.com/deploy/postgres-migration.html#migrate-the-data). Double check the content of this file before and the upstream doc before continuing. The main difference is that the upstream docs uses a dot in variables names and this breaks pgloader's mustache templating.

* Install a fresh instance of Mattermost. New instances use PostgreSQL by default. You don't need to stop or delete your existing MySQL installation.

* In the new instance, open a [Web Terminal](/apps/#web-terminal).

* Create a file named `/tmp/context.ini` and fill it up like below. You can get the MySQL connection information of your old installation by running `env | grep CLOUDRON_MYSQL` in the old instance. You can get the PostgreSQL connection information of your new installation by running `env | grep CLOUDRON_POSTGRESQL` in your new instance.

```
[pgloader]
mysql_user=f1b2b6873b4dad41
mysql_password=f1c879c56c68abb03f77ef26e20b5898fc9bfeb2f87ae358
mysql_address=mysql:3306
source_schema=f1b2b6873b4dad41

pg_user=userbec36139a3c04f7a97394a5348d4ea87
pg_password=d307edc09a68a3a8cd6b7d4a614a4c5bcb2dbe98bef0b7c86ceab55a97691f15c9c246e82e01b2e9f1db44d8d9de0c58cad3de13c169fafcaace2c7b289c6033
postgres_address=postgresql:5432
target_schema=dbbec36139a3c04f7a97394a5348d4ea87
```

* Run the migration:

```
$ pgloader --context=/tmp/context.ini /app/pkg/migration.load
```

* If the above works, migrate other directories like `files`, `plugins` from the old instance to the new instance. You can do this using the [File Manager](/apps/#file-manager)'s download and upload functionality. Plugin specific migration is covered [in upstream docs](https://docs.mattermost.com/deploy/postgres-migration.html#playbooks).

* Restart mattermost and test the whole installation before moving your new installation to the desired location.

### Bulk Export/Import

This approach is more portable and [safer](https://github.com/mattermost/mattermost/issues/18351#issuecomment-1200847434).

Known limitations:

* plugins, integrations, webhook, custom message formats, system settings have to be [setup again](https://github.com/mattermost/mattermost/issues/18351#issuecomment-1200870881)

* User passwords are not migrated. All users have to reset their password in the new instance.

Steps to migrate are very similar to [Migrating an External Instance](#migrate-external-instance):

* Export the existing MySQL installation using the same procedure as [Export External Instance](#export-external-instance).

* Install a fresh instance of Mattermost. New instances use PostgreSQL by default. You don't need to stop or delete your existing MySQL installation.

* In the new instance, import using the same procedure as [Import to Cloudron Instance](#import-to-cloudron-instance).

* Note that you have to reset the password to login in the new instance. Once you are satisfied that data is intact, you can move over the app to your existing location. We recommend stopping the MySQL instance and moving it to another location and keeping it around for some time.

