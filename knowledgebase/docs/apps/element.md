# <img src="/img/element-logo.png" width="25px"> Element App

## Setup

Element is a front end that lets you connect to Matrix home servers. See the [Synapse](/apps/synapse)
package for installing a home server.

This app is pre-configured to use the matrix installation at `matrix.yourdomain.com`.
For example, if you installed Element at `element.example.com`, the application is pre-configured
to use `matrix.example.com`.

You can change the homeserver location, by using a [Web Terminal](/apps/#web-terminal)
and editing `/app/data/config.json`.

## Custom configuration

You can add custom Element configuration using the
[Web terminal](/apps#web-terminal):

* Add any custom configuration in `/app/data/config.json`.
* Restart the app

See [config.json](https://github.com/vector-im/riot-web/blob/develop/docs/config.md) for
reference.

## Custom files

Custom files can be located under `/app/data/custom`. They can then be used in some of
the configurations (like background) as follows:

```

    "branding": {
        "welcomeBackgroundUrl": "/custom/background.png",
        "authHeaderLogoUrl": "/custom/header.png"
    }

```

