# <img src="/img/limesurvey-logo.png" width="25px"> LimeSurvey App

## About

LimeSurvey is the most versatile online survey tool for newcomers and professionals. 

* Questions? Ask in the [Cloudron Forum - LimeSurvey](https://forum.cloudron.io/category/71/limesurvey)
* [LimeSurvey Website](https://www.limesurvey.org)
* [LimeSurvey forum](https://forums.limesurvey.org/)

## Command Line Tool

The CLI tool can run using the [Web Terminal](/apps#web-terminal) as follows:

```
sudo -E -u www-data php /app/code/application/commands/console.php
```

## Templates

Questionnaire templates can be downloaded from the LimeSurvey site [here](https://account.limesurvey.org/downloads/category/19-templates).

