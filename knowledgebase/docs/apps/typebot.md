# <img src="/img/typebot-logo.png" width="25px"> Typebot App

## About

Typebot is a visual chatbot builder that helps you create chatbots for your website without coding.

* Questions? Ask in the [Cloudron Forum - Typebot](https://forum.cloudron.io/category/173/typebot)
* [Typebot Website](https://typebot.io/)
* [Typebot Issues](https://github.com/baptisteArno/typebot.io/issues)

## Custom Configuration

Typebot supports various [configuration](https://docs.typebot.io/self-hosting/configuration)
parameters that can be set as environment variables. You can edit `/app/data/env.sh` using
the [File Manager](/apps/#file-manager) to set custom configuration.

For example:

```
export NEXT_PUBLIC_UNSPLASH_APP_NAME = Whatever_Name_I_Gave_This_App_On_Unsplash	
export NEXT_PUBLIC_UNSPLASH_ACCESS_KEY = theRandomStringThatUnsplashGaveAsAnAPIkey
```

Be sure to restart the app after making any changes.

## Media Uploads

Typebot requires S3 compatible storage access to store [media uploads](https://docs.typebot.io/self-hosting/configuration#s3-storage-media-uploads).

An example configuration using minio is described below.

* Install minio and create a bucket named `typebot`.
* Configure the bucket as outlined in [typebot docs](https://docs.typebot.io/self-hosting/guides/s3):
    * In the bucket's policy, add an `Anonymous` access rule for the prefix `public/`. 
        <center>
        <img src="/img/typebot-bucket-access.png" class="shadow" width="500px">
        </center>
    * minio allows CORS requests from all endpoints by default. Thus, `CORS policy` setting can be skipped
* Create access keys for bucket access in minio
* Set the access keys for typebot by editing `/app/data/env.sh` using the [File manager](/apps/#file-manager):

```
export S3_ACCESS_KEY=rEbfwwqVZvMfBko2GuGx
export S3_SECRET_KEY=o69OUZsJD2bv1SBh0eTRJVSrtOu2VqrlxUau99Ye
export S3_BUCKET=typebot
export S3_ENDPOINT=minio-api.cloudron.example
export S3_SSL=true
export S3_REGION=us-east-1
```

* Restart Typebot


