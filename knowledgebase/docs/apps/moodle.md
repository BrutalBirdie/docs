# <img src="/img/moodle-logo.png" width="25px"> Moodle App

## About

Moodle is the world's most popular learning management system. Start creating your online learning site in minutes! 

* Questions? Ask in the [Cloudron Forum - Moodle](https://forum.cloudron.io/category/102/moodle)
* [Moodle Website](https://moodle.org/)
* [Moodle forum](https://moodle.org/course/)

## Updates

Moodle is a complex application with a complicated upgrade procedure. It supports
over 25 different [types of plugins](https://docs.moodle.org/dev/Plugin_types) each
located in a different location in the source code. While we have automated the upgrade,
do not use any more plugins than necessary to reduce update issues. On the same note,
do not edit the source code of core moodle since it will be overwritten on an update.

### File structure

For [security reasons](/security/#app-isolation-and-sandboxing) , Cloudron deploys apps
in a readonly filesystem. This means that the application code is unmodifiable. All application
data is stored in a special directory `/app/data`.

Unfortunately, Moodle is not deployable into a readonly file system since it has way too
many plugins and directories that it requires to be writable. As a workaround, we deploy
Moodle into a writable directory named `/app/data/moodle` . The actual Moodle data is stored
at `/app/data/moodledata`. On Cloudron, we rely on app source code being readonly to perform
reliable updates. As mentioned earlier, this is not possible with Moodle. Therefore, it is important to
note that Moodle source code under `/app/data/moodle` while editable should not be changed and treated
as readonly. All changes will be lost across updates. The only files that can be changed are the various
plugin and theme directories.

When performing an update, Cloudron will move the old version to `/app/data/moodle-prev-do-not-touch`
and the latest version will be at `/app/data/moodle` . As is clear with the directory name,
you should not modify anything inside `/app/data/moodle-prev-do-not-touch`.

## Themes

To install a theme, extract the new theme under `/app/data/moodle/theme` using the
[File manager](/apps/#file-manager). Then, complete the installation by going to
`Site Administration` in Moodle.

