# <img src="/img/koel-logo.png" width="25px"> Koel App

## About

Koel is a personal music streaming server.

* Questions? Ask in the [Cloudron Forum - Koel](https://forum.cloudron.io/category/109/koel)
* [Koel Website](https://koel.dev)
* [Koel issue tracker](https://github.com/koel/koel/issues)

