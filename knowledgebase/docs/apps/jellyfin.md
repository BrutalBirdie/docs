# <img src="/img/jellyfin-logo.png" width="25px"> Jellyfin App

## About

Jellyfin is the volunteer-built media solution that puts you in control of your media. Stream to any device from your own server, with no strings attached. Your media, your server, your way.

* Questions? Ask in the [Cloudron Forum - Jellyfin](https://forum.cloudron.io/category/85/jellyfin)
* [Jellyfin Website](https://jellyfin.org/)
* [Jellyfin issue tracker](https://github.com/jellyfin/jellyfin/issues/)

## Hardware Transcoding

!!! note "Cloudron 5.6 required"
    Cloudron 5.6 is the first release that supports hardware transcoding.

Jellyfin supports [hardware acceleration](https://jellyfin.org/docs/general/administration/hardware-acceleration/)
on Linux - Nvidia NVDEC, VA API and Intel QuickSync. Cloudron does not support Nvidia at the time of this
writing.

There are various steps to check if your hardware supports transcoding and if Jellyfin is able to take
advantage of it.

* Check the output of `vainfo` on your server. You might have to run `apt-get install vainfo libva2 i965-va-driver`
  if that command is not available on your server. The output should look like below. `VAEntrypointVLD` means that
    your card is capable to decode this format, `VAEntrypointEncSlice` means that you can encode to this format.

```
$ vainfo 
error: can't connect to X server!
libva info: VA-API version 1.1.0
libva info: va_getDriverName() returns 0
libva info: Trying to open /usr/lib/x86_64-linux-gnu/dri/i965_drv_video.so
libva info: Found init function __vaDriverInit_1_1
libva info: va_openDriver() returns 0
vainfo: VA-API version: 1.1 (libva 2.1.0)
vainfo: Driver version: Intel i965 driver for Intel(R) CherryView - 2.1.0
vainfo: Supported profile and entrypoints
      VAProfileMPEG2Simple            :	VAEntrypointVLD
      VAProfileMPEG2Simple            :	VAEntrypointEncSlice
      VAProfileMPEG2Main              :	VAEntrypointVLD
      VAProfileMPEG2Main              :	VAEntrypointEncSlice
      VAProfileH264ConstrainedBaseline:	VAEntrypointVLD
      VAProfileH264ConstrainedBaseline:	VAEntrypointEncSlice
      VAProfileH264Main               :	VAEntrypointVLD
      VAProfileH264Main               :	VAEntrypointEncSlice
      VAProfileH264High               :	VAEntrypointVLD
      VAProfileH264High               :	VAEntrypointEncSlice
      VAProfileH264MultiviewHigh      :	VAEntrypointVLD
      VAProfileH264MultiviewHigh      :	VAEntrypointEncSlice
      VAProfileH264StereoHigh         :	VAEntrypointVLD
      VAProfileH264StereoHigh         :	VAEntrypointEncSlice
      VAProfileVC1Simple              :	VAEntrypointVLD
      VAProfileVC1Main                :	VAEntrypointVLD
      VAProfileVC1Advanced            :	VAEntrypointVLD
      VAProfileNone                   :	VAEntrypointVideoProc
      VAProfileJPEGBaseline           :	VAEntrypointVLD
      VAProfileJPEGBaseline           :	VAEntrypointEncPicture
      VAProfileVP8Version0_3          :	VAEntrypointVLD
      VAProfileVP8Version0_3          :	VAEntrypointEncSlice
      VAProfileHEVCMain               :	VAEntrypointVLD
```

* Enable Hardware transcoding in Jellyfin. Admin Dashboard > Playback > Transcoding.

    <center>
    <img src="/img/jellyfin-enable-transcoding.png" class="shadow" width="500px">
    </center>

* Next step is to check the [Jellyfin logs](https://jellyfin.org/docs/general/administration/hardware-acceleration/#verifying-transcodes) of the name `FFmpeg.Transcode-*`. This can be found at Admin Dashboard > Logs. They are also at `/app/data/jellyfin/log`. Please note that if this file does not exist, it's probably not transcoding.

* Finally, when the video is playing, open a new browser tab and see the `Active Devices` in the Emby dashboard. Click on 'i' for transcoding information.

