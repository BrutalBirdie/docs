# <img src="/img/hedgedoc-logo.png" width="25px"> HedgeDoc App

## About

HedgeDoc is the best platform to write and share markdown.

* Questions? Ask in the [Cloudron Forum - HedgeDoc](https://forum.cloudron.io/category/38/hedgedoc)
* [HedgeDoc Website](https://demo.hedgedoc.org/)
* [HedgeDoc issue tracker](https://github.com/hedgedoc/hedgedoc/issues)

!!! note "Impending rename"
    CodiMD is being renamed to HedgeDoc. This app package will be renamed accordingly.

## Custom configuration

Use the [File manager](/apps#file-manager)
to place custom configuration under `/app/data/config.json`.

See [HedgeDoc docs](https://docs.hedgedoc.org/configuration/)
for configuration options reference.

## Image uploads

By default, images are uploaded to the data directory of the app itself.
To switch to another provider like MinIO, first [configure minio](https://github.com/hedgedoc/hedgedoc/blob/master/docs/guides/minio-image-upload.md) to image uploads. Then, use the following configuration in
`/app/data/config.json`:

```
    "imageUploadType": "minio",
    "s3bucket": "codimd-images",
    "minio": {
        "accessKey": "MINIO_ACCESS_KEY",
        "secretKey": "MINIO_SECRET_KEY",
        "endPoint": "minio.cloudrondomain.com",
        "secure": true,
        "port": 443
    }
```

## CLI Tool

The CLI tool `bin/manage_users` can be used to manage users. Open a [Web Terminal](/apps/#web-terminal):

```
root@8b1237e8-6ae8-4dd8-8bce-22b404c03f9d:/app/code# export CMD_DB_URL="${CLOUDRON_POSTGRESQL_URL}"
root@8b1237e8-6ae8-4dd8-8bce-22b404c03f9d:/app/code# bin/manage_users 
You did not specify either --add or --del or --reset!

Command-line utility to create users for email-signin.

Usage: bin/manage_users [--pass password] (--add | --del) user-email
	Options:
		--add 	Add user with the specified user-email
		--del 	Delete user with specified user-email
		--reset Reset user password with specified user-email
		--pass	Use password from cmdline rather than prompting
```

